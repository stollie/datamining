package hr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 *
 * @author Remco
 */
public class Categorie extends Object {
    public String name;
    public boolean targetsCounted = false;
    private ArrayList<String> options = new ArrayList<String>();
    private ArrayList<String> optionTargets = new ArrayList<String>();
    private HashSet<String> uniqueOptions = new HashSet<String>();
    private ArrayList<String> uniqueOptionsList = new ArrayList<String>();

    private HashSet<Dag> dagen = new HashSet<Dag>();

    // ov, fiets in ons voorbeeld
    public String[] targetsNames;
    // som van ov & fiets
    public int[] targetsCounts; // Hoe vaak komt er een waarde voor de optie OV of FIETS

    public int[][] values;      // Alle waardes opslaan, omdat er ook nog std.dev. berekend moet worden.
    public int[] sums;          // maak bij het inlezen direct een som van alle waarde, zodat sneller mean kan uitrekenen
    public double[] means;       // de mean kan denk ik toch berkend worden.
    public double[] stdDevs;     // std.dev. opgeslagen per optie
    
    public int[][] catCounts; // Dag en fietsofov hebben deze niet
    
    private double totaleFout;
    // Als target dan moet de likelihood anders berekend worden.
    private boolean target;
    
    public Categorie (String name, String[] targets, boolean isTarget) {
        this.name           = name;
        // huidige geval  OV of FIETS
        this.targetsNames   = targets;
        this.target         = isTarget;
        this.targetsCounts  = new int[this.targetsNames.length];
        
        this.values     = new int[this.targetsNames.length][0];
        this.sums       = new int[this.targetsNames.length];
        
        this.means      = new double[this.targetsNames.length];
        this.stdDevs    = new double[this.targetsNames.length];
    }

    public void addDag (Dag dag) {
        this.dagen.add(dag);
    }

    public HashSet getDagen () {
        return this.dagen;
    }
    
    /**
     * Een string waarde toevoegen
     * @param value
     * @param target
     */
    public void addCatData (String value, String target) {
        this.options.add(value);
        this.optionTargets.add(target);
        // alleen unieke items worden toegevoegd.
        this.uniqueOptions.add(value);
        // Aantal waardes voor de optie OV of FIETS bijhouden
        this.targetsCounts[Arrays.asList(this.targetsNames).indexOf(target)]++;
    }
    /**
     * Een int waarde toevoegen
     * @param value
     * @param target
     */
    public void addIntData (String value, String target) {
        if (value.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+")) {
            int indexID = Arrays.asList(this.targetsNames).indexOf(target);
            int curSum = this.sums[indexID];
            
            // Elke waarde opslaan
            //make array 1 spot bigger
            //System.out.println(this.values[indexID].length);
            int[] tempArray = new int[this.values[indexID].length + 1];
            System.arraycopy(this.values[indexID], 0, tempArray, 0, this.values[indexID].length);
            this.values[indexID] = tempArray;
            //add new value
            this.values[indexID][this.values[indexID].length-1] = Integer.parseInt(value);
            
            this.sums[indexID] = curSum + Integer.parseInt(value);
            //this.countValues[Arrays.asList(this.targetsNames).indexOf(target)]++;
        }
    }

    /**
     * Bereken de waarnemingen
     */
    public void count() {
        this.targetsCounted = true;
        this.uniqueOptionsList = new ArrayList<>(uniqueOptions);
        System.out.println(":: COUNT " + this.name );
        /* Voorbeeld
       +--------+-ov-+-fiets-+-laagste fout-+
       | warm   |  1 |  1    |  1
       | koud   |  4 |  3    |  3
       +--------+----+-------+
        */
        // unieke regels, met de cols met de target count
        // init de array [aantal categorieen][aantal targets]
        this.catCounts = new  int[this.uniqueOptionsList.size()][(this.targetsNames.length+1)];
        // Tellen, alle waardes in de matix zetten
        for (int i = 0; i < this.options.size(); i++) {
            // [categorie] [target] ++;
            this.catCounts[this.uniqueOptionsList.indexOf(this.options.get(i))][Arrays.asList(this.targetsNames).indexOf(this.optionTargets.get(i))]++;
        }
    }
    /**
     * Kleinste fout
     */
    public void getNumberLikelihood () {
        if (false == this.targetsCounted) {
            System.out.println("Error: Eerst count uitvoeren");
        }
        else {
            // Weergeven
            int tellerNaam = 0;
            int totaalFout = 0;
            // Uitzoeken per regel welke target (ov / fiets) het laagst is.
            System.out.println("  | "+this.targetsNames[0] + " " +this.targetsNames[1] + " | fout ");

            for (int[] integer : this.catCounts) {
                // Welke is de laagste fout? opslaan in de laatste col
                if (integer[0] > integer[1]) {
                    integer[2] = integer[1];
                    totaalFout += integer[1];
                }
                else {
                    integer[2] = integer[0];
                    totaalFout += integer[0];
                }
                System.out.println((integer[0]+integer[1])+" | " + integer[0]+" "+integer[1]+" | "+integer[2] + " <- " +this.uniqueOptionsList.get(tellerNaam));
                tellerNaam += 1;
            }
            System.out.println("  | = = |");
            System.out.println("  | "+this.targetsCounts[0]+ " "+ this.targetsCounts[1]);

            this.totaleFout = ((double)totaalFout/(double)this.options.size());

            System.out.println("Kleinste fout "+totaalFout+ "/"+this.options.size()+ " -> " + this.totaleFout);

            /// ----
            // Bereken de numerieke likelihood
            for (int i = 0; i < this.targetsNames.length; i++) {
                // De mean
                this.means[i] = this.sums[i] / this.targetsCounts[i];
                // std.Devs. uitrekenen
                for (int j = 0; j < this.values[i].length; j++) {
                    //System.out.print("("+this.values[i][j] +" - "+ this.means[i]+")*2 = " + (this.values[i][j] - this.means[i])*2);
                    //this.stdDevs[i] += (this.values[i][j] - this.means[i])*2;
                    this.stdDevs[i] += ((this.values[i][j] - this.means[i])*(this.values[i][j] - this.means[i]));
                    //System.out.println(" nu: "+this.stdDevs[i]);
                }
                this.stdDevs[i] = Math.sqrt(this.stdDevs[i]/this.targetsCounts[i]);
                System.out.println("Gemiddelde["+i+"]: " + this.means[i] + " std.dev.: "+ this.stdDevs[i]);
            }
        }
    }
    
    /**
     * getLikelihood op basis van categorie
     * @param optie
     * @param target
     * @return 
     */
    public double getLikelihood (String optie, String target) {
        //System.out.print(":: getLikelihood - ");
        //System.out.print("Optie: " + optie + " Target: " + target);
        // de keuze ophalen
        double keuze = this.catCounts[this.uniqueOptionsList.indexOf(optie)][Arrays.asList(this.targetsNames).indexOf(target)];
        double totaal;
        if (this.target) {
            //System.out.print("-> ");
            totaal = this.options.size();
        }
        else {
            // het totaal ophalen
            //totaal = this.catCounts[this.uniqueOptionsList.size()][Arrays.asList(this.targetsNames).indexOf(target)];
            totaal = this.targetsCounts[Arrays.asList(this.targetsNames).indexOf(target)];
        }
        //System.out.println(" = " + keuze + " / " +totaal +" = "+keuze/totaal);
        return keuze/totaal;
    }
    
    public double getLikelihood (int optie, String target) {
        //System.out.print(":: getNumerLikelihood: ");
        //System.out.println("Optie: " + optie + " Target: " + target);
        return this.getChance(optie, Arrays.asList(this.targetsNames).indexOf(target));
    }
    
    public double getTotaleFout () {
        return this.totaleFout;
    }
    
    public double getChance(int value, int columnIndex) {
        //System.out.println("columnIndex: "+ columnIndex);
        //if(this.stddev.length == 0) {
        //    this.calculateStdDev();
        //}
        
        double x            = value;
        double thisMean     = this.means[columnIndex];
        double thisStddev   = this.stdDevs[columnIndex];
        double stddevPow    = thisStddev*2;
        //System.out.println("thisMean: "+ thisMean+" thisStddev: "+ thisStddev);
        double answer = 0;
        
        if(stddevPow != 0) {
//            System.out.println("--kans dichtheid formule voor een normaal verdeling: ");
//            System.out.println("(x): "+ (x));
//            System.out.println("(p): "+ (thisMean));
//            System.out.println("(x-p): "+ (x-thisMean));
//            System.out.println("(x-p/O): "+ (x-thisMean)/thisStddev);
//            System.out.println("(x-p/O)*2: "+ (((x-thisMean)/thisStddev)*2));
//            System.out.println("e- 0.5*((x-p)/O)*2: "+ Math.pow(Math.E, (-0.5*(((x-thisMean)/thisStddev)*2))));
//            System.out.println("1 / O * w(2*pi) : "+ (1/(thisStddev*Math.sqrt(2*Math.PI))));
            
            //answer = (1/(thisStddev*Math.sqrt(2*Math.PI))) * Math.pow(Math.E, -0.5*(((x-thisMean)/thisStddev)*2));
            answer = (1/(thisStddev*Math.sqrt(2*Math.PI))) * Math.pow(Math.E, -0.5*(((x-thisMean)/thisStddev)*((x-thisMean)/thisStddev)));
        }
        //System.out.println("getChance: "+ answer);
        //System.out.println("-----");
        return answer;
    }
}
