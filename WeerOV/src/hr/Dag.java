package hr;
/**
 *
 * @author Josephine
 */
public class Dag {
    public int id;
    public int tempratuur;
    public int windkracht;
    public int neerslagkans;
    public boolean metFiets;
    public String ovoffiets;
    public String[] data;
    public String[] catData;
    public String target;

    /**
     *
     */
    public Dag (String[] data) {
        // Nog oplossing voor al input leeg is.
        this.id             = Integer.parseInt(data[0]);
        this.tempratuur     = Integer.parseInt(data[1]);
        this.windkracht     = Integer.parseInt(data[2]);
        this.neerslagkans   = Integer.parseInt(data[3]);
        this.ovoffiets      = data[4];
        this.metFiets       = data[4].equals("FIETS");
        this.data           = data;
        this.target         = this.ovoffiets;
        //this.data[3]        = Integer.toString(Integer.parseInt(this.data[3])/10);
        // Data set in categorieen
        this.catData    = new String[5];
        this.catData[0] = data[0];
        this.catData[1] = this.tempratuurCategorie(this.tempratuur);
        this.catData[2] = this.windkrachtCategorie(this.windkracht);
        this.catData[3] = this.neerslagkansCategorie(this.neerslagkans);
        this.catData[4] = data[4];
    }
    
    /**
     * Wordt dit nog wel gebruikt :/
     * @param id
     * @param tempratuur
     * @param windkracht
     * @param neerslagkans
     * @param metFiets
     */
    public Dag ( String id, String tempratuur, String windkracht, String neerslagkans, String metFiets) {
        this.id             = Integer.parseInt(id);
        this.tempratuur     = Integer.parseInt(tempratuur);
        this.windkracht     = Integer.parseInt(windkracht);
        this.neerslagkans   = Integer.parseInt(neerslagkans);
        this.ovoffiets      = metFiets;
        this.metFiets       = metFiets.equals("FIETS");
    }
    
    public String neerslagkansCategorie(int neerslagkans) {
        // 0 geen, 10-30 klein, 40-60 gemiddeld, 70-90 groot, 100 zeker
        if (neerslagkans == 0) {
            return "geen";
        } else if (neerslagkans <= 30) {
            return "klein";
        } else if (neerslagkans <= 60) {
            return "gemiddeld";
        } else if (neerslagkans <= 90) {
            return "groot";
        } else {
            return "zeker";
        }
    }

    public String windkrachtCategorie(int windkracht) {
        //zwak <3,matig <5, krachtig <7 , hard
        if (windkracht < 3) {
            return "zwak";
        } else if (windkracht < 5) {
            return "matig";
        } else if (windkracht < 7) {
            return "krachtig";
        } else {
            return "hard";
        }
    }

    public String tempratuurCategorie(int tempratuur) {
        //=ALS(B5>20;"hot";ALS(B5>10;"mild";"cool"))s;
        if (tempratuur < 10) {
            return "koud";
        } else if (tempratuur < 20) {
            return "gemiddeld";
        } else {
            return "warm";
        }
    }
    
    public double windkrachtBeperken() {
        //zwak <3,matig <5, krachtig <7 , hard
        double value = 12;
        if (this.windkracht < 3) {
            value = 3;
        } else if (this.windkracht < 5) {
            value = 6;
        } else if (this.windkracht < 7) {
            value = 9;
        }
        return value;
    }
    /**
     * 
     * @return 
     */
    public double tempratuurBeperken() {
        //=ALS(B5>20;"hot";ALS(B5>10;"mild";"cool"))s;
        double value = 12;
        if (this.tempratuur < 10) {
            value= 4;
        } else if (this.tempratuur < 20) {
            value = 8;
        }
        return value;
    }
    
    /**
     * 0 geen, 10-30 klein, 40-60 gemiddeld, 70-90 groot, 100 zeker
     * @return 
     */
    public double neerslagkansBeperken() {
        double kans = 1;
        if (this.neerslagkans == 0) {
            kans = 3;
        } else if (this.neerslagkans <= 30) {
            kans = 6;
        } else if (this.neerslagkans <= 60) {
            kans = 9;
        } else if (this.neerslagkans <= 90) {
            kans = 12;
        }
        return kans;
    }
}
