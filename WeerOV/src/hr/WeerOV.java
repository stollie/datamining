package hr;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Remco
 */
public class WeerOV {

    public List<Dag> dagen = new ArrayList<>();
    public List<String> colnames = new ArrayList<>();
    public List<Categorie> cats = new ArrayList<>();
    /** 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        WeerOV obj = new WeerOV();
        obj.run();
        obj.calculate();
        obj.oneR();
        //obj.zeroR("fietsofov");
        
        obj.Treemaker();
        //obj.likeHood("koud", "matig", "klein");
        //obj.likeHood("8", "7" , "100");
        //obj.likeHood("20", "2", "10");
    }
    public void Treemaker () {
        //info([2,3],[4,0],[3,2])=   14
        TreeMaker tree = new TreeMaker(this.cats, this.dagen);
        //double antwoord = tree.totaleinfo(new double[][]{{2,3},{4,0},{3,2}},14);
        //System.out.println("antwoord: " + antwoord);
    }

    /**
     * CSV uitlezen en een Dag object maken van elke regel.
     */
    public void run() {
        File file;
        file = new File("src/data/weer_case_OV_fiets.csv");
        // Check for Remco :)
        if (false == file.exists()) {
            file = new File("WeerOv/src/data/weer_case_OV_fiets.csv");
        }
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
            br = new BufferedReader(new FileReader(file.getAbsolutePath()));
            int counter  = 0;
            while ((line = br.readLine()) != null) {
                counter++;
                // use comma as separator
                String[] weer = line.split(cvsSplitBy);
                // make the categories with the first row of the file.
                if (counter == 1) {
                    for (int i = 0; i < weer.length; i++) {
                        this.colnames.add(weer[i]);
                        if (i < (weer.length-1)) {
                            // een array: new String[]{"FIETS","OV"} meegeven zodat tagets direct bekend zijn.
                            this.cats.add(new Categorie(weer[i],new String[]{"FIETS","OV"}, false));
                        }
                        else {
                            // last col is target
                            this.cats.add(new Categorie(weer[i],new String[]{"FIETS","OV"}, true));
                        }
                    }
                }
                else {
                    this.dagen.add(new Dag(weer));
                    Dag dag = this.dagen.get(this.dagen.size()-1);
                    addData(dag);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    /**
     * addCatData = Uitlezen van categorie data en toevoegen aan de categorie
     * @param dag
     */
    public void addData (Dag dag) {
        int counter = 0;
        // Add cat data
        for (String data : dag.catData){
            Categorie cat = this.cats.get(counter);
            cat.addDag(dag);
            //System.out.println(data);
            cat.addCatData(data, dag.target);
            counter++;
        }
        // Add number data
        counter = 0;
        for (String data : dag.data){
            //System.out.println(data);
            Categorie cat = this.cats.get(counter);
            cat.addIntData(data, dag.target);
            counter++;
        }
    }
    /**
     * Calculate
     * dag en fietsofov col uitsluiten
     */
    public void calculate () {
        for (Categorie cat : this.cats) {
            if (
                false == cat.equals(this.cats.get(0)) && // Dag niet berekenen
                false == cat.equals(this.cats.get((this.cats.size()-1)) ) // FIETSOFOV niet berekenen
            ) {
                // Tel alles
                cat.count();
            }
        }

    }
    /**
     * Laat OneR zien.
     */
    public void oneR () {
        double laagsteFout  = 1;
        Categorie laagsteCat = this.cats.get(0);
        for (Categorie cat : this.cats) {
            if (cat.targetsCounted) {
                cat.getNumberLikelihood();
                if (cat.getTotaleFout() < laagsteFout) {
                    laagsteFout = cat.getTotaleFout();
                    laagsteCat = cat;
                }
            }
        }
        System.out.println("---Laagste fout---");
        System.out.println(laagsteFout * 100 + "%");
        System.out.println(laagsteCat.name);
        System.out.println("------");
    }

    /**
     * ZeroR
     * @param col de colname
     */
    public void zeroR(String col) {
        System.out.println("ZeroR van: " + col);
        // De id van de col
        int colId       = this.colnames.indexOf(col);
        double size     = (double)this.dagen.size();
        Categorie cat   = this.cats.get(colId);
        //System.out.println(cat.name);

        for (int i = 0 ; i < cat.targetsCounts.length; i++) {
            System.out.println(cat.targetsNames[i] + ": " + cat.targetsCounts[i] + " = " +  (double)cat.targetsCounts[i] / size * 100 + "%");
        }
    }

    /**
     * 
     * @param temperatuur opties: koud gemiddeld warm
     * @param windkracht opties: zwak matig krachtig hard
     * @param neerslagkans opties: geen klein gemiddeld groot zeker
     * @param vervoerskeuze opties: ov fiets
     */
    //public void likeHood(String temperatuur, String windkracht, String neerslagkans, String vervoerskeuze) {
    public void likeHood(String temp, String wind, String neerslag) {
        //int colsCount = this.colnames.size();
        Categorie temperatuurCat    = this.cats.get(1);
        Categorie windkrachtCat     = this.cats.get(2);
        Categorie neerslagkansCat   = this.cats.get(3);
        Categorie fietsofovCat      = this.cats.get(4);

        String sTemp        = temp;
        String sWind        = wind;
        String sNeerslag    = neerslag;
        
        //boolean numbers     = false;
        int iTemp       = 0;
        int iWind       = 0;
        int iNeerslag   = 0;
        
        double kansTem;
        double kansWind;
        double kansNeerslag;
        double kansTem2;
        double kansWind2;
        double kansNeerslag2;
        if (
            temp.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+") &&
            wind.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+") &&
            neerslag.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+")
        ) {
            System.out.println("--- Met nummers rekenen ---");
            //numbers = true;
            iTemp = Integer.parseInt(temp);
            iWind = Integer.parseInt(wind);
            iNeerslag = Integer.parseInt(neerslag);
        
            // Wat zijn de kansen met het OV
            kansTem = temperatuurCat.getLikelihood(iTemp , "OV");
            kansWind = windkrachtCat.getLikelihood(iWind , "OV");
            kansNeerslag = neerslagkansCat.getLikelihood(iNeerslag, "OV");
            // Nu die fiets controleren
            kansTem2 = temperatuurCat.getLikelihood(iTemp, "FIETS");
            kansWind2 = windkrachtCat.getLikelihood(iWind, "FIETS");
            kansNeerslag2 = neerslagkansCat.getLikelihood(iNeerslag, "FIETS");
        }
        else {
            System.out.println("--- Met categorieen rekenen ---");
            // Wat zijn de kansen met het OV
            kansTem = temperatuurCat.getLikelihood(sTemp , "OV");
            kansWind = windkrachtCat.getLikelihood(sWind , "OV");
            kansNeerslag = neerslagkansCat.getLikelihood(sNeerslag, "OV");
            // Nu die fiets controleren
            kansTem2 = temperatuurCat.getLikelihood(sTemp, "FIETS");
            kansWind2 = windkrachtCat.getLikelihood(sWind, "FIETS");
            kansNeerslag2 = neerslagkansCat.getLikelihood(sNeerslag, "FIETS");
        }
        double kansVervoer = fietsofovCat.getLikelihood("OV", "OV");
        double kansVervoer2 = fietsofovCat.getLikelihood("FIETS", "FIETS");
        
        System.out.println("- Kans met OV");
        System.out.print(kansTem + " * " + kansWind + " * " + kansNeerslag + " * " + kansVervoer + " = ");
        double likeHood = kansTem * kansWind * kansNeerslag * kansVervoer;
        System.out.println(likeHood);
        
        System.out.println("- Kans met FIETS");
        System.out.print(kansTem2 + " * " + kansWind2 + " * " + kansNeerslag2 + " * " + kansVervoer2 + " = ");
        double likeHood2 = kansTem2 * kansWind2 * kansNeerslag2 * kansVervoer2;
        System.out.println(likeHood2);
        System.out.println("- Kans op FIETS: "+likeHood2+" / ("+likeHood2+" + " + likeHood+")*100 = ");
        System.out.println(likeHood2 / (likeHood2 + likeHood)*100 + "%");
        System.out.println("- Kans op OV: "+likeHood+" / ("+likeHood2+" + " + likeHood+")*100 = ");
        System.out.println(likeHood / (likeHood2 + likeHood)*100 + "%");
    }
}
