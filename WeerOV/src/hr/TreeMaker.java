package hr;

import java.util.List;

/**
 * @author Josephine
 */
public class TreeMaker {

    public TreeMaker(List <Categorie> cats, List<Dag> dagen) {
        System.out.println("#####" );
        //this.Test();
        System.out.println(":: TreeMaker" );
        Categorie hoogstecat = cats.get(0);
        double hoogsteGainRatio = 0;
        double som = 14;
        for(Categorie cat : cats){
            if (cat.targetsCounted) {
                System.out.println((double)cat.targetsCounts[0] + " " + (double)cat.targetsCounts[1] );
                System.out.println(cat.name);
                
                double targetInfo = this.info((double)cat.targetsCounts[0], (double)cat.targetsCounts[1]);
                System.out.println("Target info: "+targetInfo);

                double totaleInfo = this.totaleinfo(cat.catCounts, 14);
                System.out.println("Totale info: " +totaleInfo);
                
                double gain = (targetInfo - totaleInfo);
                System.out.println("Gain: " + gain);
                
                double splitInfo = this.splitInfo(cat.catCounts);
                System.out.println("split info: " +splitInfo);

                double gainRatio = (gain / splitInfo);
                System.out.println("Gain ratio: " + gainRatio);
                System.out.println("=====");
                if (gainRatio > hoogsteGainRatio) {
                    hoogsteGainRatio = gainRatio;
                    hoogstecat = cat;
                }
            }
        }

        System.out.println("Hoogste: " + hoogstecat.name);
    }
   // public void catbouwer (List<Dag> dagen, List <Categorie> cats) {
        
    //}
    public void Test () {
        System.out.println("==TEST==");
        System.out.println("this.info(9, 5) = 0,9403");
        System.out.println(this.info(9, 5));
        System.out.println("=====");
        System.out.println("info([2,2],[4,2],[3,1]) = 0,9111[bits]");
        int[][] myIntArray = {{2,2},{4,2},{3,1}};
        System.out.println(this.totaleinfo(myIntArray, 14));
        System.out.println("=====");
        System.out.println("gain(temperature)=0,9403- 0,9111= 0,029[bits]");
        System.out.println((this.info(9, 5)-this.totaleinfo(myIntArray, 14)));
        System.out.println("=====");
        System.out.println("info([4,6,4])= 1,5566");
        System.out.println(this.info(4,6,4));
        System.out.println("=====");
        System.out.println("gain ratio = 0,029 / 1,5566 = 0,019");
        System.out.println((this.info(9, 5)-this.totaleinfo(myIntArray, 14)) / this.info(4,6,4));
        System.out.println("=======");
    }

    /**
     *
     * @param info
     * @return
     */
    public double splitInfo(int[][] info) {
        double total = 0;
        int[] sommen = new int[(info.length+1)];
        int couter = 0;
        for (int[] in : info) {
            int som = in[0] + in[1];
            sommen[couter] = som;
            System.out.println("SOM: " + som);
            couter++;
        }
        if (info.length == 3) {
            total = this.info((double)sommen[0],(double)sommen[1],(double)sommen[2]);
        }
        if (info.length == 4) {
            total = this.info((double)sommen[0],(double)sommen[1],(double)sommen[2],(double)sommen[3]);
        }
        
        return total;        
    }
    
    /*
     * som = totaal aantal waardes in de catergorie
     * @param info int[][]
     * @param som
     * @return double
     */
    public double totaleinfo(int[][] info, double som) {
        double total = 0;
        for (int[] in : info) {
            //5/14*info([2,3])
            // als alles catergorie behoort, wordt er nul teruggegeven
            if (in[0] != 0 && in[1] != 0) {
                total += (((double)in[0] + (double)in[1]) / som) * info((double)in[0], (double)in[1]); // toch even lelijk met (double)
            }
        }
        return total;
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public double info(double a, double b) {
        double info;
        double totaal = a + b;
        info = 1 / (totaal) * (-(a) * log2(a / (totaal)) - (b) * log2(b / (totaal)));
        return info;
    }
    /**
     *
     * @param a
     * @param b
     * @param c
     * @return
     */
    public double info(double a, double b, double c) {
        double info;
        double totaal = a + b + c;
        info = 1 / (totaal) * (-(a) * log2(a / (totaal)) - (b) * log2(b / (totaal)) - (c) * log2(c / (totaal)));
        return info;
    }
    /**
     *
     * @param a
     * @param b
     * @param c
     * @param d
     * @return
     */
    public double info(double a, double b, double c, double d) {
        double info;
        double totaal = a + b + c + d;
        info = ((a + b) * info(a, b) + (c + d) * info(c, d)) / totaal;
        return info;
    }
    /**
     *
     * @param x
     * @return
     */
    public double log2(double x) {
        return Math.log(x) / Math.log(2);
    }
}
